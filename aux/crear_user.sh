#!/bin/bash

#pedimos al usuario que teclee el txt con los usuarios que quiere crear
read -p "Introduce el txt que contiene el nombre de los usuarios a crear:" -r fichero
if [ ! -f "$fichero" ] #comprobación de argumento, tiene que ser un fichero
then
	echo "error, el argumento pasado no es un fichero"  #imprimimos error
else

	#bucle while que lee el txt línea a línea
	while IFS= read -r line
	do
		echo "creado el usuario $line"  
		sudo useradd "$line"  #crea el usuario
	done < "$fichero"  #en esta variable hemos cargado el txt que leeremos línea a línea
fi
