#!/bin/bash
#Comprueba si hay conexion con una ip pasada como argumento

#pedimos al usuario introducir ip a comprobar
read -p "Introduce una ip: " -r ip

#realizamos ping para comprobar conexión
ping -c 2 "$ip"

#si la ejecución de ping es correcta ejecutamos la linea dentro del then
#si la ejecución de ping devuelve error ejectuamos la linea dentro del else
if [ $? -eq 0 ] #esta linea comprueba si la salida del ping es correcta o devuelve error
then
	echo "Hay conexión física con la ip: $ip"
else
	echo "No hay conexión con la ip: $ip"
fi

