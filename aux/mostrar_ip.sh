#!/bin/bash
#Este script muestra la direccion IP de tu ordenador

#obtener la IP utilizando el comando 'ip'
ip_address=$(ip -4 addr show | grep -oP '(?<=inet\s)\d+(\.\d+){3}' )

#verificar si se encontro la ip
if [ -z "$ip_address" ]
then
	#mensaje de error
	echo "error: no se pudo determinar la IP"
else
	#muestra ip
	echo "La dirección IP de tu ordenador es: $ip_address"
fi
