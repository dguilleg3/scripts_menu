#!/bin/bash
#pedimos al usuario que teclee el fichero que desee 
read -p "Introduce el txt que contiene el nombre de los directorios que desea crear:" -r archivo
#pedimos al usuario que teclee donde quiere situar los nuevos directorios
#indicamos que teclee .. ya que nos situamos en la carpeta aux y el usuario ejecuta el menu desde la carpeta menu que contiene a la carpeta aux, de todos los .sh auxiliares
#si quiere asignar otra ubicación, indicamos que asigne la ruta absoluta para que no haya errores
read -p "Introduce el directorio donde se situará el nuevo directorio creado:
	 Si desea crear el directorio en la carpeta actual indique ..
       	 En caso contrario introduce ruta absoluta
" -r ruta
if [ ! -f "$archivo" ] #comprueba si el argumento es un fichero
then
	echo "error, argumento inválido. Por favor introduce un fichero" #mensaje de error
else
	while IFS= read -r line  #bucle que lee el fichero linea a linea
	do 
		echo "creado el directorio $ruta/$line"  
		mkdir "$ruta"/"$line"  #crea directorios
	done < "$archivo" #aqui se indica fichero que se leera
fi
