#!/bin/bash

#pedimos al usuario que introduzca el txt que incluya los directorios a eliminar
read -p "Introduce el txt que contiene el nombre de los directorios a eliminar (con su ruta abosluta): " -r archivo
if [ ! -f "$archivo" ]  #comprobamos si el argumento pasado es un fichero 
then
	echo "el argumento pasado no es un fichero de texto" #mensaje de error
else 
	while IFS= read -r line #bucle while que lee nuestro fichero linea a linea
	do
		if [ ! -d "$line" ] #comprobamos si el argumento pasado es un directorio
		then
			echo "$line no es un directorio" #mensaje de error
		else
			echo "eliminado el directorio $line"
			rmdir "$line"   #elimina directorio
		fi
	done < "$archivo" #aqui nombramos al txt que será leído linea a línea
fi









