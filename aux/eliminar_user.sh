#!/bin/bash

#pedimos al usuario que teclee el fichero que desee
read -p "Introduce el txt que contiene el nombre de los usuarios a eliminar: " -r fichero

if [ ! -f "$fichero" ]
then
	echo "error, el argumento no es un fichero"
else
	while IFS= read -r line
	do
		echo "eliminado el usuario $line"
		sudo userdel -r "$line"
	done < "$fichero"
fi
