#!/bin/bash

#indicamos al usuario que nos indique si quiere hacer una copia de seguridad de un fichero o carpeta
#si nos indica 1 será a un fichero y si nos indica 2 a un directorio
#respuesta cargada en la variable opcion
echo "introduce 1 si quiere relizar una copia de un fichero."
echo "introduce 2 si quiere realizar una copia de directorio."
read -r opc  #guardamos la eleccion del user en la variable opc
#bucle con opciones 1, 2 u otro valor
#en funcion del valor de la variable opcion creara copia de fichero, de directorio o dara mensaje de error
case "$opc" in
	1)
		#crear copia a fichero
		echo "Indica el nombre del archivo a duplicar, con su ruta absoluta: "
	        read -r archivo
	        if [ ! -f "$archivo" ]  #comprobación argumento
		then
			#mensaje de error
			echo"error, el valor insertado no corresponde a un fichero"
		else	
			echo "Indica directorio con ruta absoluta donde realizar copia: "
	        	read -r destino
			if [ ! -d  "$destino" ]   #comprobacion argumento
			then
				#mensaje error
				echo "error, el valor insertado no corresponde a un directorio"
			else
				cp "$archivo" "$destino"   #crea copia
				echo "copia de seguridad del fichero creada en $destino"
			fi
		fi
	;;

	2)
		#crear copia a directorio
		echo "Indica el directorio a duplicar: "
		read -r directorio
		if [ ! -d "$directorio" ] #comprobacion argumentos
		then
			#mensaje de error
			echo "error, el valor insertado no se corresponde con un directorio"
		else

			echo "Indica directorio con ruta abosluta donde realizar copia: "
			read  -r destino2
			if [ ! -d "$destino2" ] #comprobacion argumentos
			then
				#mensaje de error
				echo " error, el valor insertado no se corresponde con un directorio"
			else
				cp -r "$directorio" "$destino2"  #crea copia
			fi
		fi
	;;

	*)
		#argumento distinto de 1 y 2, mensaje de error
		echo "argumento inválido"
	;;
esac

