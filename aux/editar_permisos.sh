#!/bin/bash

#solicita un directorio y permisos que queremos asignarle a ese directorio para asignarselos

#solicitar directorio
read -p "Ingresa el directorio con su ruta absoluta: " -r directorio

#comprobar si el directorio existe
if [ -d "$directorio" ]
then
	#solicita permisos en octal
	read -p "Ingresa los nuevos permisos para el directorio (en octal): " -r permisos
	
	#cambia permisos
	sudo chmod "$permisos" "$directorio"
	echo "Permisos del directorio $directorio cambiados a $permisos"
else
	#mensaje de error
	echo "El valor introducido no es un directorio"
fi


