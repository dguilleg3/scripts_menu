#!/bin/bash

#saludo al usuario que este conectado al ordenador

#variables para el saludo
user=$(whoami)
fecha=$(date +"%d/%m/%Y")
hora=$(date +"%H:%M:%S")

#saludo impreso por pantalla
echo "Bienvenido al menu $user, hoy día $fecha a las $hora"
#bucle para volver al menu hasta presionar el 10 (exit)
while true
do
	#opciones menu
	echo "Seleccione el valor de la opción deseada"
	echo "1. Crear usuario"
	echo "2. Eliminar usuario"
	echo "3. Crea directorios"
	echo "4. Crear copia de seguridad"
	echo "5. Editar permisos directorio"
	echo "6. Eliminar directorio"
	echo "7. Mostrar IP"
	echo "8. Renovar IP"
	echo "9. Comprobar conexion con ping"
	echo "10. Salir"

	echo "Introduzca el valor númerico de la opción"
	read -r option
	case $option in
		#llamada a scripts auxiliares de cada opcion del menu
		#enlace de cada script auxiliar con su número de opción (case option)
		#el nombre de las funciones indica su funcionalidad
		1)	./aux/crear_user.sh			
			;;
		2)	./aux/eliminar_user.sh		
			;;
		3)	./aux/crear_directorio.sh	
			;;
		4)	./aux/copia_seguridad.sh
			;;
		5)	./aux/editar_permisos.sh
			;;
		6)	./aux/eliminar_directorio.sh
			;;
		7)	./aux/mostrar_ip.sh
			;;
		8)	./aux/renovar_ip.sh
			;;
		9)	./aux/ping.sh
			;;
		10)	#opcion 10 para salir del menú y del bucle con exit 1
			echo "exit";
			exit
			;;
		*)	#opción valor no valido, no comprendido entre 1-10
		       	echo "La opcion introducida no es válida.";
			;;
	esac
done

